import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlbumComponent } from './components/album/album.component';
import { ArtistComponent } from './components/artist/artist.component';
import { ArtistPageComponent } from './pages/artist-page/artist-page.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateAlbumComponent } from './pages/create-album page/create-album/create-album.component';
import { MenuComponent } from './shared/menu/menu.component';
import { AlbumIdComponent } from './components/album/album-id/album-id/album-id.component';
import { AlbumPageComponent } from './pages/album-page/album-page.component';
import { AlbumComponentComponent } from './shared/componentes/album-component/album-component.component';
import { FiterAlbumComponent } from './components/album/filter-album/fiter-album/fiter-album.component';
import { ArtistComponentComponent } from './shared/componentes/artist-component/artist-component/artist-component.component';
import { UpdateComponent } from './pages/update-page/update/update.component';


@NgModule({
  declarations: [
    AppComponent,
    AlbumComponent,
    ArtistComponent,
    ArtistPageComponent,
    CreateAlbumComponent,
    MenuComponent,
    AlbumIdComponent,
    AlbumPageComponent,
    AlbumComponentComponent,
    FiterAlbumComponent,
    ArtistComponentComponent,
    UpdateComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
