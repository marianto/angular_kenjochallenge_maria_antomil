import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-album-component',
  templateUrl: './album-component.component.html',
  styleUrls: ['./album-component.component.scss']
})
export class AlbumComponentComponent implements OnInit {

  @Input() album;
  @Output () deleteClickEmitter = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deleteAlbum(idAlbum){
    this.deleteClickEmitter.emit(idAlbum);
    }
}
