import { Component, OnInit } from '@angular/core';
import { AlbumService } from 'src/app/services/album.service';
import {FormBuilder, Validators, FormGroup} from'@angular/forms';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { take } from "rxjs/operators";
import { ArtistService } from 'src/app/services/artist.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.scss']
})
export class CreateAlbumComponent implements OnInit {

  public formGroupAlbum;
  public formGroupArtist;
  album;
  constructor(private formBuilder:FormBuilder, private albumService:AlbumService, private httpClient:HttpClient, private artistService:ArtistService, private route:ActivatedRoute) { }

  ngOnInit(): void {

    this.formGroupAlbum = this.formBuilder.group ({
      title: ['Title',[Validators.required]],
      //artistId: ['artistId'],//
      coverUrl: ['coverUrl'],
      year: ['2020',[Validators.required]],
      genre: ['Genre',[Validators.required]],
    })

    this.formGroupArtist = this.formBuilder.group({
      name: ['name',[Validators.required]],
      photoUrl: ['photoUrl'],
      
    })
  }

  //addAlbum(){
  //  console.log(this.formGroupAlbum.value);
  //  this.albumService.createAlbum(this.formGroupAlbum.value).pipe(take(1)).subscribe(() => {});
  //}
  createAlbum(){
   console.log(this.formGroupAlbum.value);
   const httpOptions ={
   headers: new HttpHeaders({
    'Content-Type':'application/json'
     })
   }
    return this.httpClient.post(environment.url + '/album', this.formGroupAlbum.value, httpOptions).pipe(take(1)).subscribe(() => {});
      
  }



  createArtist(){
    console.log(this.formGroupArtist.value);
    const httpOptions ={
    headers: new HttpHeaders({
     'Content-Type':'application/json'
      })
    }
     return this.httpClient.post(environment.url + '/artist', this.formGroupArtist.value, httpOptions).pipe(take(1)).subscribe(() => {});
       
   }

}
