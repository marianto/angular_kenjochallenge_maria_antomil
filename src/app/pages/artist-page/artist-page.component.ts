import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ArtistService } from 'src/app/services/artist.service';

@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss']
})
export class ArtistPageComponent implements OnInit {


@Input () artists = [];
@Output() deleteClickEmitter = new EventEmitter();
  constructor(private artistService:ArtistService) { }

  ngOnInit(): void {
  }

  deleteArtist(idArtist){
    this.deleteClickEmitter.emit(idArtist);
  }

}
