import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumIdComponent } from './components/album/album-id/album-id/album-id.component';
import { AlbumComponent } from './components/album/album.component';
import { ArtistComponent } from './components/artist/artist.component';
import { CreateAlbumComponent } from './pages/create-album page/create-album/create-album.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

const routes: Routes = [
  {
    path: '', component: HomePageComponent,
  },
  {
    path: 'artists', component: ArtistComponent,
  },
  {
    path: 'artist/:idArtist', component: AlbumIdComponent,
  },
{
  path: 'albums', component: AlbumComponent,
},
{
  path: 'album/:idAlbum', component: AlbumIdComponent,
},
{
  path: 'create', component: CreateAlbumComponent,
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
